package com.senghuy.firebaseservice;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthProvider;

public class LoginActivity extends AppCompatActivity {

    private final static String TAG = LoginActivity.class.getName();

    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;

    private MaterialButton mButtonLogInWithGoogle;
    private Button mButtonLogInWithPhoneNo;
    private EditText mEditTextPhoneNo;

    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    // init auth with facebook
    private CallbackManager mCallbackManager;
    private FirebaseAuth mFirebaseAuth;
    private LoginButton mButtonLoginWithFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mButtonLogInWithPhoneNo = findViewById(R.id.button_log_with_phone);
        mButtonLogInWithPhoneNo.setOnClickListener(v -> {
            Log.d(TAG, "Test Button Login with Phone: ");
        });
        
        // init UIs
        initUIs();

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // bind events with Google
        onButtonLogInWithGoogleClicked();

        // bind events Phone No
        onLoginWithPhoneNo();

       // Bind events with FaceBook
        onLoginWithFacebook();


        // Validate Text phone
        onInputPhoneNumber();
    }

    // set  Text phone
    private void onInputPhoneNumber() {
        mButtonLogInWithPhoneNo = findViewById(R.id.button_log_with_phone);
        mEditTextPhoneNo = findViewById(R.id.editTextPhoneNo);
        mButtonLogInWithPhoneNo.setEnabled(false);
        mEditTextPhoneNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length()<7){
                    mButtonLogInWithPhoneNo.setEnabled(false);
                } else {
                    mButtonLogInWithPhoneNo.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    // set events with FaceBook
    private void onLoginWithFacebook() {
        // init auth with Facebook
        mFirebaseAuth = FirebaseAuth.getInstance();
        FacebookSdk.sdkInitialize(getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();
        mButtonLoginWithFacebook = findViewById(R.id.buttonLoginWithFacebook);
        mButtonLoginWithFacebook.setReadPermissions("email", "public_profile");
        mButtonLoginWithFacebook.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(@NonNull FacebookException e) {
                Log.d(TAG, "facebook:onError", e);
            }
        });
    }

    // set onActivityResult to Auth Facebok
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    // START auth_with_facebook
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }
    // [END auth_with_facebook]

    private void updateUI(FirebaseUser user) {

    }

    // init with PhoneNo
    private void onLoginWithPhoneNo() {
        mButtonLogInWithPhoneNo = findViewById(R.id.button_log_with_phone);
        mButtonLogInWithPhoneNo.setOnClickListener(v -> {
            mEditTextPhoneNo = findViewById(R.id.editTextPhoneNo);
            Intent intent = new Intent(getApplicationContext(), PhoneVerifyActivity.class);
            intent.putExtra("phoneNo", mEditTextPhoneNo.getEditableText().toString());
            startActivity(intent);
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            // already login
            User myUser = new User(user.getEmail(), user.getDisplayName());
            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
            mainIntent.putExtra("my_user", myUser);
            startActivity(mainIntent);
        }

        updateUI(user);
    }

    private void initUIs() {
        mButtonLogInWithGoogle = findViewById(R.id.buttonLoginWithGoogle);
    }

    private void onButtonLogInWithGoogleClicked() {
        mButtonLogInWithGoogle.setOnClickListener(view -> {
            logInWithGoogle();
        });
    }

    private void logInWithGoogle() {
        Intent intent = mGoogleSignInClient.getSignInIntent();
        logInLauncher.launch(intent);
    }

    private ActivityResultLauncher<Intent> logInLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(result.getData());
                    try {
                        // Google Sign In was successful, authenticate with Firebase
                        GoogleSignInAccount account = task.getResult(ApiException.class);
                        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());
                        firebaseAuthWithGoogle(account.getIdToken());
                    } catch (ApiException e) {
                        // Google Sign In failed, update UI appropriately
                        Log.w(TAG, "Google sign in failed", e);
                    }
                }
            }
    );

    private void firebaseAuthWithGoogle(String idToken) {

        AuthCredential credential =GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = mAuth.getCurrentUser();
                            Log.d(TAG, "Email = " + user.getEmail());
                            Log.d(TAG, "Name = " + user.getDisplayName());
                            User myUser = new User(user.getEmail(), user.getDisplayName());
                            Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
                            mainIntent.putExtra("my_user", myUser);
                            startActivity(mainIntent);
                            //updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //updateUI(null);
                        }
                    }
                });

    }

}